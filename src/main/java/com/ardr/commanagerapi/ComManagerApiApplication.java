package com.ardr.commanagerapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ComManagerApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ComManagerApiApplication.class, args);
	}

}
