package com.ardr.commanagerapi.entity;

import com.ardr.commanagerapi.enums.ComputerIsGood;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@Setter
public class Computer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = true)
    private LocalDate upgradeDate;

    @Column(nullable = false)
    private Boolean computerIsUp;

    @Column(nullable = true)
    private String upContent;

    @Column(nullable = false)
    private Boolean computerIsCheck;

    @Column(nullable = true)
    private String checkContent;

    @Enumerated(value = EnumType.STRING)
    private ComputerIsGood computerIsGood;

    @Column(columnDefinition = "TEXT")
    private String etcMemo;

}
