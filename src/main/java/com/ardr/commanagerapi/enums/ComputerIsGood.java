package com.ardr.commanagerapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ComputerIsGood {

    BREAK_DOWN("고장"),
    GOOD("정상"),
    CHECKING("점검중"),
    NEED_CHECK("점검 필요"),
    UNDER_REPAIR("수리중");

    private final String name;

}
